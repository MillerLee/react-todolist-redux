# Todo List Basic with Redux
This project template was built with [Create React App](https://github.com/facebookincubator/create-react-app).


## Preview
![react-todo-redux.jpg](http://i.imgur.com/DmQ8tID.jpg)

## How to use

Clone the repo and install the dependencies.

```bash
$ git clone git@github.com:ruanyf/react-babel-webpack-boilerplate.git <yourAppName>
$ cd <yourAppName>
$ npm install
$ npm start
```

navigate to `localhost:3000` on your browser

## Structure


```
TODO_LIST
├─ public
│   ├─ todobg.jpg
│   ├─ index.css
│   └─ index.html
│
├─ src
│   ├─ action
│   │   └─ index.js
│   ├─ components
│   │   ├─ App.js
│   │   ├─ Header.js
│   │   ├─ Footer.js
│   │   ├─ Link.js
│   │   ├─ Todo.js
│   │   └─ TodoList.js
│   │
│   ├─ containers
│   │   ├─ AddTodo.js
│   │   ├─ FilterLink.js
│   │   ├─ UndoRedo.js
│   │   └─ VisibleTodoList.js
│   │
│   ├─ reducers
│   │   ├─ index.js
│   │   ├─ todos.js
│   │   └─ visibilityFilter.js
│   │
│   └─index.js
│
├─ .gitignore
├─ LICENSE
├─ package.json
└─ README.md
```


## Available Scripts

In the project directory, you can run:

```bash
$ npm start
```

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


The page will reload if you make edits.
You will also see any lint errors in the console.

```bash
$ npm run build
```

Builds the app for production to the `build` folder.

It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.

Your app is ready to be deployed!


- - -


You can see more script in `package.json`




## App structure

// ing